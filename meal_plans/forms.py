from django.forms import ModelForm

from meal_plans.models import MealPlan


class MealPlanForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = ["name", "recipes", "date"]


class MealPLanDeleteForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = []
